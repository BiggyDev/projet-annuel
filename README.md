# AdventureWorks Products Management

AdventureWorks Products Management is a platform where you can see all products stored in database and add more products thanks to a simple form.

### Informations
* This project works with **[Docker-Compose](https://docs.docker.com/compose/install/)**.

### Installation

#### Installation and container launch from *command line*.

+ First, please clone the project :
````bash
git clone https://gitlab.com/BiggyDev/projet-annuel.git
````

+ Move forward inside the project folder :
````bash
cd rattrapage-projet-annuel
````

+ Launch the container :

````bash
docker-compose up -d
````
