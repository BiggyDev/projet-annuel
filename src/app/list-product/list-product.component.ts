import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Product} from "../models/Product";
import {MatSort, Sort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {ProductService} from "../services/product.service";
import {LiveAnnouncer} from "@angular/cdk/a11y";

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.sass']
})
export class ListProductComponent implements OnInit {

  today = new Date();
  displayedColumns: string[] = [
    'Name',
    'ProductNumber',
    'Origin',
    'Salable',
    'Color',
    'SafetyStockLevel' ,
    'ReorderPoint',
    'StandardCost',
    'ListPrice',
    'Size',
    'Weight',
    'DaysToManufacture',
    'ProductLine',
    'Class',
    'Style',
    'SubcategoryName',
    'ModelName',
    'CatalogDescription',
    'Instructions',
    'SellStartDate',
    'SellEndDate',
    'DiscontinuedDate',
    'ModifiedDate'
  ];
  dataSource: MatTableDataSource<Product> = new MatTableDataSource<Product>();
  @ViewChild(MatSort, {static: false}) sort: MatSort | undefined;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator | undefined;
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private productService: ProductService,
              private _liveAnnouncer: LiveAnnouncer) {}

  ngOnInit() {
    this.getAllProducts();
  }

  getAllProducts(): void {
    this.productService.getAllProducts().subscribe((products: Product[]) => {
      this.dataSource = new MatTableDataSource<Product>(products);
      this.dataSource.sort = this.sort || null;
      this.dataSource.paginator = this.paginator || null;
      this.isLoadingResults = false;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Announce the change in sort state for assistive technology. */
  announceSortChange(sortState: Sort) {
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

}
