import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {AddProductComponent} from "./add-product/add-product.component";
import {ListProductComponent} from "./list-product/list-product.component";

const routes: Routes = [
  {
    path: '',
    component: ListProductComponent
  },
  {
    path: 'add-product',
    component: AddProductComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
