import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../services/product.service";
import {Product} from "../models/Product";
import {SubcategoryService} from "../services/subcategory.service";
import {ModelService} from "../services/model.service";
import {Subcategory} from "../models/Subcategory";
import {Model} from "../models/Model";
import * as moment from 'moment';
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.sass']
})
export class AddProductComponent implements OnInit {
  addProductForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    number: new FormControl('', [Validators.required]),
    origin: new FormControl(1, [Validators.required]),
    salable: new FormControl(1, [Validators.required]),
    color: new FormControl(null),
    safetyStockLevel: new FormControl(0, [Validators.required]),
    reorderPoint: new FormControl(0, [Validators.required]),
    standardCost: new FormControl(0, [Validators.required]),
    listPrice: new FormControl(0, [Validators.required]),
    size: new FormControl(null),
    sizeUnitMeasureCode: new FormControl(null),
    weightUnitMeasureCode: new FormControl(null),
    weight: new FormControl(null),
    daysToManufacture: new FormControl(0, [Validators.required]),
    productLine: new FormControl(null),
    class: new FormControl(null),
    style: new FormControl(null),
    productSubcategory: new FormControl(null),
    productModel: new FormControl(null),
    startSellDate: new FormControl(new Date(), [Validators.required]),
    endSellDate: new FormControl(null),
    discontinuedDate: new FormControl(null)
  })
  subcategories?: Subcategory[];
  models?: Model[];
  characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  constructor(private productService: ProductService,
              private subcategoryService: SubcategoryService,
              private modelService: ModelService) { }

  ngOnInit(): void {
    this.getAllSubcategories();
    this.getAllModels();
  }

  getAllSubcategories(): void {
    this.subcategoryService.getAllSubcategories().subscribe((subcategories) => {
      this.subcategories = subcategories;
    })
  }

  getAllModels(): void {
    this.modelService.getAllModels().subscribe((models) => {
      this.models = models;
    })
  }

  addProduct(): void {
    let product = new Product(
      this.addProductForm.controls['name'].value,
      this.addProductForm.controls['number'].value,
      this.addProductForm.controls['origin'].value,
      this.addProductForm.controls['salable'].value,
      this.addProductForm.controls['color'].value,
      this.addProductForm.controls['safetyStockLevel'].value,
      this.addProductForm.controls['reorderPoint'].value,
      this.addProductForm.controls['standardCost'].value,
      this.addProductForm.controls['listPrice'].value,
      this.addProductForm.controls['size'].value,
      this.addProductForm.controls['sizeUnitMeasureCode'].value,
      this.addProductForm.controls['weightUnitMeasureCode'].value,
      this.addProductForm.controls['weight'].value,
      this.addProductForm.controls['daysToManufacture'].value,
      this.addProductForm.controls['productLine'].value,
      this.addProductForm.controls['class'].value,
      this.addProductForm.controls['style'].value,
      this.addProductForm.controls['productSubcategory'].value,
      this.addProductForm.controls['productModel'].value,
      moment(this.addProductForm.controls['startSellDate'].value).format('YYYY-MM-DD hh:mm:ss'),
      this.addProductForm.controls['endSellDate'].value !== null ? moment(this.addProductForm.controls['endSellDate'].value).format('YYYY-MM-DD hh:mm:ss') : null,
      this.addProductForm.controls['discontinuedDate'].value !== null ? moment(this.addProductForm.controls['discontinuedDate'].value).format('YYYY-MM-DD hh:mm:ss') : null,
      moment().format('YYYY-MM-DD hh:mm:ss'),
      this.generateRandomString()
    );

    this.productService.addProduct(product).subscribe(res => {
      console.log("Product Added");
    })
  }

  generateRandomString() {
    let result = '';
    const charactersLength = this.characters.length;
    for ( let i = 0; i < 14; i++ ) {
      result += this.characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    result = '0x' + result

    return result;
  }

}
