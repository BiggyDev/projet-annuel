export class Subcategory {
  ProductSubcategoryID: number;
  ProductCategoryID: number;
  SubcategoryName: string;
  ModifiedDate: Date;

  constructor(ProductSubcategoryID: number, ProductCategoryID: number, SubcategoryName: string, ModifiedDate: Date) {
    this.ProductSubcategoryID = ProductSubcategoryID === undefined ? 0 : ProductSubcategoryID;
    this.ProductCategoryID = ProductCategoryID === undefined ? 0 : ProductCategoryID;
    this.SubcategoryName = SubcategoryName === undefined ? '' : SubcategoryName;
    this.ModifiedDate = ModifiedDate === undefined ? new Date() : ModifiedDate;
  }
}
