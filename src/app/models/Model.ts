export class Model {
  ProductModelID: number;
  ModelName: string;
  CatalogDescription: string;
  Instructions: string;
  ModifiedDate: Date;

  constructor(ProductModelID: number, ModelName: string, CatalogDescription: string, Instructions: string, ModifiedDate: Date) {
    this.ProductModelID = ProductModelID === undefined ? 0 : ProductModelID;
    this.ModelName = ModelName === undefined ? '' : ModelName;
    this.CatalogDescription = CatalogDescription === undefined ? '' : CatalogDescription;
    this.Instructions = Instructions === undefined ? '' : Instructions;
    this.ModifiedDate = ModifiedDate === undefined ? new Date() : ModifiedDate;
  }
}
