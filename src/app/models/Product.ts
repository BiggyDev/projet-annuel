import {MakeFlag} from "./MakeFlag";
import {FinishedGoodsFlag} from "./FinishedGoodsFlag";

export class Product {
  Name: string;
  ProductNumber: string;
  MakeFlag: MakeFlag;
  FinishedGoodsFlag: FinishedGoodsFlag;
  Color: null | string;
  SafetyStockLevel: number;
  ReorderPoint: number;
  StandardCost: number;
  ListPrice: number;
  Size: null | string;
  SizeUnitMeasureCode: null | string;
  WeightUnitMeasureCode: null | string;
  Weight: null | number;
  DaysToManufacture: number;
  ProductLine: null | string;
  Class: null | string;
  Style: null | string;
  ProductSubcategoryID: null | number;
  ProductModelID: null | number;
  SellStartDate: string;
  SellEndDate: null | string;
  DiscontinuedDate: null | string;
  ModifiedDate: string;
  rowguid: string;

  constructor(Name: string, ProductNumber: string, MakeFlag: MakeFlag, FinishedGoodsFlag: FinishedGoodsFlag, Color: string | null, SafetyStockLevel: number, ReorderPoint: number, StandardCost: number, ListPrice: number, Size: string | null, SizeUnitMeasureCode: string | null, WeightUnitMeasureCode: string | null, Weight: number | null, DaysToManufacture: number, ProductLine: string | null, Class: string | null, Style: string | null, ProductSubcategoryID: number | null, ProductModelID: number | null, SellStartDate: string, SellEndDate: string | null, DiscontinuedDate: string | null, ModifiedDate: string, rowguid: string) {
    this.Name = Name;
    this.ProductNumber = ProductNumber;
    this.MakeFlag = MakeFlag;
    this.FinishedGoodsFlag = FinishedGoodsFlag;
    this.Color = Color === null ? null : Color;
    this.SafetyStockLevel = SafetyStockLevel;
    this.ReorderPoint = ReorderPoint;
    this.StandardCost = StandardCost;
    this.ListPrice = ListPrice;
    this.Size = Size === null ? null : Size;
    this.SizeUnitMeasureCode = SizeUnitMeasureCode === null ? null : SizeUnitMeasureCode;
    this.WeightUnitMeasureCode = WeightUnitMeasureCode === null ? null : WeightUnitMeasureCode;
    this.Weight = Weight === null ? null : Weight;
    this.DaysToManufacture = DaysToManufacture;
    this.ProductLine = ProductLine === null ? null : ProductLine;
    this.Class = Class === null ? null : Class;
    this.Style = Style === null ? null : Style;
    this.ProductSubcategoryID = ProductSubcategoryID === null ? null : ProductSubcategoryID;
    this.ProductModelID = ProductModelID === null ? null : ProductModelID;
    this.SellStartDate = SellStartDate;
    this.SellEndDate = SellEndDate === null ? null : SellEndDate;
    this.DiscontinuedDate = DiscontinuedDate === null ? null : DiscontinuedDate;
    this.ModifiedDate = ModifiedDate;
    this.rowguid = rowguid;
  }
}
