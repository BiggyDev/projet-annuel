import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Product} from "../models/Product";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  baseUrl = 'http://localhost:3080/products';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + '/', this.httpsOptions);
  }

  public addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.baseUrl + '/', product, this.httpsOptions)
  }
}
