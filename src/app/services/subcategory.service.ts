import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Subcategory} from "../models/Subcategory";

@Injectable({
  providedIn: 'root'
})

export class SubcategoryService {
  baseUrl = 'http://localhost:3080/subcategories';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public getAllSubcategories(): Observable<Subcategory[]> {
    return this.http.get<Subcategory[]>(this.baseUrl + '/', this.httpsOptions);
  }
}
