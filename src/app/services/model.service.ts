import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Model} from "../models/Model";

@Injectable({
  providedIn: 'root'
})

export class ModelService {
  baseUrl = 'http://localhost:3080/models';
  httpsOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public getAllModels(): Observable<Model[]> {
    return this.http.get<Model[]>(this.baseUrl + '/', this.httpsOptions);
  }
}
